
/*
Escriba una consulta que muestre los montos pendientes de las Cuentas por Cobrar (Clientes)
y Cuentas por Pagar (Proveedores), agrupado por los grupos de Socios de Negocios.
Se deben cumplir las siguientes condiciones:
✓ Las columnas deben ser:
    o TipodeGrupodeSociodeNegocio.
    o Nombre del Grupo de Socio de Negocio.
    o Monto Total pendiente de la Deuda (según tipo de cuenta).
    o Montoparcialdeladeudahasta30días(segúntipodecuenta).
    o Montoparcialdeladeudadesde30,hasta60días(segúntipodecuenta). o Montoparcialdeladeudamayora60días(segúntipodecuenta).
        ✓ Debe estar ordenado por Tipo de Grupo de Socio de Negocios y por Nombre del Grupo, el segundo de forma descendente.
        ✓ El formato de los campos numéricos debe ser DECIMAL, con dos cifras de precisión después del punto decimal.
        ✓ Los nombres de grupos deben estar en formato Titulo (Ej.: Empresas Grandes).
        ✓ Incluir un subtotal por Tipo de Grupo de Socio de Negocio.
        ✓ Debe existir un parámetro de entrada opcional, para el Tipo de Grupo de Socio de Negocio.
        ✓ Tomar en cuenta únicamente los documentos de código 13,14 y 24.
*/
USE MOVIMIENTOS_BANCARIOS
GO

SELECT  
        COALESCE(CASE
            WHEN GROUP_COMPANY.GROUPNAME IS NOT NULL THEN GROUP_COMPANY.GROUPTYPE
            ELSE 'Sub Total ' + GROUP_COMPANY.GROUPTYPE
        END, 'Total General') AS 'Tipo de Grupo',

        COALESCE(GROUP_COMPANY.GROUPNAME, '') AS 'Nombre de Grupo',

        SUM(COALESCE(TRANS.BALDUECRED, 0) + COALESCE(TRANS.BALDUEDEB, 0)) AS 'Monto Deuda',

        SUM(CASE 
            WHEN DATEDIFF(DAY, REFDATE, GETDATE()) < 30 THEN COALESCE(TRANS.BALDUECRED, 0) + COALESCE(TRANS.BALDUEDEB, 0)
            ELSE 0
        END) AS 'Monto a 0 - 30 dias',

        SUM(CASE 
            WHEN DATEDIFF(DAY, REFDATE, GETDATE()) BETWEEN 30 AND 60 THEN COALESCE(TRANS.BALDUECRED, 0) + COALESCE(TRANS.BALDUEDEB, 0)
            ELSE 0
        END) AS 'Monto a 30 - 60 dias',

        SUM(CASE 
            WHEN DATEDIFF(DAY, REFDATE, GETDATE()) > 60 THEN COALESCE(TRANS.BALDUECRED, 0) + COALESCE(TRANS.BALDUEDEB, 0)
            ELSE 0
        END) AS 'Monto > 60 dias'

FROM JDT1 TRANS
INNER JOIN OCRD CLIENT ON CLIENT.CARDCODE = TRANS.SHORTNAME
INNER JOIN OCRG GROUP_COMPANY ON GROUP_COMPANY.GROUPCODE = CLIENT.GROUPCODE
GROUP BY GROUP_COMPANY.GROUPTYPE, GROUP_COMPANY.GROUPNAME WITH ROLLUP
