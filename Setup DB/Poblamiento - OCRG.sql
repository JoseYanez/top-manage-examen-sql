USE MOVIMIENTOS_BANCARIOS
GO

INSERT INTO OCRG (GROUPNAME, GROUPTYPE)
    VALUES  
        ('Empresas Grandes', 'Cliente'),
        ('Empresas Pequeñas', 'Cliente'),
        ('Internacionales', 'Supplier'),
        ('Servicios Públicos', 'Supplier')
GO

