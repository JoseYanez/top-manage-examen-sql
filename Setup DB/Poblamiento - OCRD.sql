USE MOVIMIENTOS_BANCARIOS
GO

INSERT INTO OCRD (GROUPCODE, CARDNAME)
    VALUES
        (1, 'Empresa G1'),
        (1, 'Empresa G2'),
        (1, 'Empresa G3'),

        (2, 'Empresa P1'),
        (2, 'Empresa P2'),
        (2, 'Empresa P3'),
        (2, 'Empresa P4'),
        (2, 'Empresa P5'),
        (2, 'Empresa P6'),

        (3, 'Empresa S1 Internacional 1'),
        (3, 'Empresa S2 Internacional 2'),
        (4, 'Empresa S3 Servicio Público 1'),
        (4, 'Empresa S4 Servicio Público 2')

